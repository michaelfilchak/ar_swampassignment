using Game.Scripts.Core;
using Game.Scripts.UtilsAndExts;
using UnityEngine;

namespace Game.Scripts.Services {
    public class PcCameraControlApiService : ApiServiceMonobeh {
        #region Unity

        [SerializeField]
        private float moveSpeed = 10;
        [SerializeField]
        private float rotationSensitivity = 2;
        [SerializeField]
        private int scrollSensitivity = 1;

        private void Awake() {
            gameObject.SetActive(Utils.Application.IsEditorOrStandalone);
        }

        private void Update() {
            // XZ-Axis
            var deltaSpeed = moveSpeed * Time.deltaTime;
            var axisVertical = Input.GetAxis("Vertical");
            var axisHorizontal = Input.GetAxis("Horizontal");
            var forward = Vector3.ProjectOnPlane(targetCameraTransform.forward, Vector3.up).normalized;
            var targetCameraPosition = targetCameraTransform.position;
            targetCameraPosition += forward * (axisVertical * deltaSpeed) + targetCameraTransform.right * (axisHorizontal * deltaSpeed);

            { // Y-Axis
                var mouseScrollDelta = (int)Input.mouseScrollDelta.y;

                if (mouseScrollDelta != 0) {
                    targetCameraPosition.y += mouseScrollDelta * scrollSensitivity;
                    targetCameraPosition.y = targetCameraPosition.y < 1 ? 1 : targetCameraPosition.y > 20 ? 20 : targetCameraPosition.y;
                }
            }

            targetCameraTransform.position = targetCameraPosition;

            // Rotate around XY-Axis
            if (Input.GetMouseButton(1)) {
                var targetEulerAngles = targetCameraTransform.eulerAngles;
                var mouseX = Input.GetAxis("Mouse X");
                var mouseY = Input.GetAxis("Mouse Y");
                targetEulerAngles.x += -mouseY * rotationSensitivity;
                targetEulerAngles.x = targetEulerAngles.x < 1 ? 1 : targetEulerAngles.x > 89 ? 89 : targetEulerAngles.x;
                targetEulerAngles.y += mouseX * rotationSensitivity;
                targetEulerAngles.z = 0;
                targetCameraTransform.eulerAngles = targetEulerAngles;
            }
        }

        #endregion

        #region Public

        public override void Init() {
            base.Init();
            targetCameraTransform = Api.Resolve<CameraApiService>().CachedCamera.transform;
        }

        #endregion

        #region Private

        private Transform targetCameraTransform;

        #endregion
    }
}