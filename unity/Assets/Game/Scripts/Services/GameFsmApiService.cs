using Game.Scripts.Core;
using Game.Scripts.GameFsm;

namespace Game.Scripts.Services {
    public class GameFsmApiService : ApiService {
        #region Public

        public override void Init() {
            base.Init();
            gameFsm.SetApi(Api);
        }

        public void Launch() {
            gameFsm.Launch<MainMenuFsmState>();
        }

        #endregion

        #region Private

        private readonly Fsm<GameFsmState> gameFsm = new Fsm<GameFsmState>();

        #endregion
    }
}