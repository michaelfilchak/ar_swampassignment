﻿using Game.Scripts.Core;
using UnityEngine;

namespace Game.Scripts.Services {
    public class CameraApiService : ApiServiceMonobeh {
        #region Unity

        [SerializeField]
        private Camera cachedCamera;

        #endregion

        #region Public

        public Camera CachedCamera => cachedCamera;

        #endregion
    }
}