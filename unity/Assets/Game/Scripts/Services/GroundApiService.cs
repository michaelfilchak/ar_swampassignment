﻿using Game.Scripts.Core;
using UnityEngine;

namespace Game.Scripts.Services {
    public class GroundApiService : ApiServiceMonobeh {
        #region Unity

        [SerializeField]
        private Transform groundTransform;
        [SerializeField]
        private Material groundMaterial;

        private void Awake() {
            Hide();
        }

        private void OnDestroy() {
            SetGridSize(1, 1);
        }

        #endregion

        #region Public

        public void Hide() {
            SetGridSize(1, 1);
            groundTransform.gameObject.SetActive(false);
        }

        public void Show(int x, int z) {
            SetGridSize(x, z);
            groundTransform.gameObject.SetActive(true);
        }

        #endregion

        #region Private

        private void SetGridSize(int x, int z) {
            groundTransform.localScale = new Vector3(x, 1, z);
            groundMaterial.mainTextureScale = new Vector2(x, z);
        }

        #endregion
    }
}