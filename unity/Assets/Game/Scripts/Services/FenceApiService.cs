﻿using System;
using System.Collections.Generic;
using System.IO;
using Game.Scripts.Core;
using Game.Scripts.Windows;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Scripts.Services {
    public class FenceApiService : ApiService {
        #region Public

        public bool IsModelReady => model != null;
        public IFenceModel FenceModel => model;
        public string LocalFencePath { get; private set; } = PlayerPrefs.GetString(LocalFencePathKey, null);

        public void CloseModel() {
            if (model != null) {
                model = null;
            } else {
                Debug.LogError("Double close model");
            }
        }

        public Vector3 ConvertTileIdToWorldCoordinates(uint value) {
            Fence.ConvertFenceTileIdToGridCoordinates(value, out var x, out var z);

            return new Vector3(x + 0.5f, 0, z + 0.5f);
        }

        public string GetJson() {
            var data = new DataV1JsonModel {
                Grid = {
                    GridSizeX = (ushort)model.GridSize.x,
                    GridSizeZ = (ushort)model.GridSize.y
                },
                Fence = {
                    TileIds = model.GetTileIdArray()
                }
            };
            var dataJson = JsonUtility.ToJson(data);

            var project = new ProjectJsonModel {
                Version = 1,
                Data = dataJson
            };

            var projectJson = JsonUtility.ToJson(project);

            return projectJson;
        }

        public bool IsFenceAreaLocked() {
            RefreshFreeTileIds();

            return (freeTileIds.Count + model.TileCount) < (model.GridSize.x * model.GridSize.y);
        }

        public bool IsFreeTileId(uint value) {
            return freeTileIds.Contains(value);
        }

        public void ProcessClickOnTile(in Vector2Int value) {
            model.ProcessClickOnTile(value);
        }

        public void RefreshFreeTileIds() {
            freeTileIds.Clear();

            var tileIdsToCheck = new Stack<uint>();

            var gridSize = model.GridSize;
            var gridSizeX = (ushort)gridSize.x;
            var gridSizeZ = (ushort)gridSize.y;
            var gridXLast = (ushort)(gridSizeX - 1);
            var gridZLast = (ushort)(gridSizeZ - 1);

            Fence.GetFenceTileId(0, 0, out var tileId);
            tileIdsToCheck.Push(tileId);
            freeTileIds.Add(tileId);

            var debugCounter = model.GridSize.x * model.GridSize.y;

            while (tileIdsToCheck.Count > 0) {
                tileId = tileIdsToCheck.Pop();
                Fence.ConvertFenceTileIdToGridCoordinates(tileId, out var tileX, out var tileZ);

                if (tileX > 0) { // left
                    var tileXLeft = (ushort)(tileX - 1);
                    Fence.GetFenceTileId(tileXLeft, tileZ, out tileId);

                    if (!model.ContainsTileId(tileId)) {
                        if (freeTileIds.Add(tileId)) {
                            tileIdsToCheck.Push(tileId);
                        }
                    }
                }

                if (tileX < gridXLast) { // right
                    var tileXRight = (ushort)(tileX + 1);
                    Fence.GetFenceTileId(tileXRight, tileZ, out tileId);

                    if (!model.ContainsTileId(tileId)) {
                        if (freeTileIds.Add(tileId)) {
                            tileIdsToCheck.Push(tileId);
                        }
                    }
                }

                if (tileZ < gridZLast) { // top
                    var tileZTop = (ushort)(tileZ + 1);
                    Fence.GetFenceTileId(tileX, tileZTop, out tileId);

                    if (!model.ContainsTileId(tileId)) {
                        if (freeTileIds.Add(tileId)) {
                            tileIdsToCheck.Push(tileId);
                        }
                    }
                }

                if (tileZ > 0) { // bottom
                    var tileZBottom = (ushort)(tileZ - 1);
                    Fence.GetFenceTileId(tileX, tileZBottom, out tileId);

                    if (!model.ContainsTileId(tileId)) {
                        if (freeTileIds.Add(tileId)) {
                            tileIdsToCheck.Push(tileId);
                        }
                    }
                }

                if (--debugCounter < 0) {
                    break;
                }
            }

            if (debugCounter < 0) {
                Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage("Something went wrong in the algorithm");
            }
        }

        public void SaveModelLocally() {
            var filename = model.FileName;

            if (string.IsNullOrEmpty(filename)) {
                filename = "localFenceModel.txt";
                model.SetFileName(filename);
            }

            var path = Path.Combine(Application.persistentDataPath, filename);
            var json = GetJson();

            try {
                File.WriteAllText(path, json);
                Debug.Log("Saved to: " + path);
                LocalFencePath = path;
                PlayerPrefs.SetString(LocalFencePathKey, LocalFencePath);
                PlayerPrefs.Save();
            } catch (Exception e) {
                Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage("Couldn't save to file " + filename);
                Debug.LogError(e);
            }
        }

        public bool TryCreateModel(in Vector2Int gridSize, bool showErrors = true) {
            if (model != null) {
                if (showErrors) {
                    Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage("Grid model is not null");
                }

                return false;
            }

            if (CheckGridSize(gridSize)) {
                model = new Fence(gridSize);

                return true;
            }

            return false;
        }

        public bool TryLoadModelFromJson(string value) {
            var project = JsonUtility.FromJson<ProjectJsonModel>(value);

            if (project.Version == 1) {
                return TryLoadProjectV1(project);
            }

            var message = $"Unknown project version ({project.Version.ToString()})";
            Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage(message);
            Debug.LogError(message);

            return false;
        }

        public bool TryLoadModelLocally(string path) {
            if (string.IsNullOrEmpty(path)) {
                Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage("Path is null");

                return false;
            }

            if (!File.Exists(path)) {
                Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage("File doesn't exist");

                return false;
            }

            string projectJson;

            try {
                projectJson = File.ReadAllText(path);
            } catch (Exception e) {
                Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage("Couldn't save to file " + path);
                Debug.LogError(e);

                return false;
            }

            return TryLoadModelFromJson(projectJson);
        }

        #endregion

        #region Private

        private const string LocalFencePathKey = "LastLocalFenceKey";
        private readonly HashSet<uint> freeTileIds = new HashSet<uint>();
        private readonly Vector2Int gridSizeMax = new Vector2Int(1000, 1000);
        private readonly Vector2Int gridSizeMin = new Vector2Int(10, 10);
        private Fence model;

        private bool CheckGridSize(in Vector2Int gridSize, bool showErrors = true) {
            if (gridSize.x < gridSizeMin.x || gridSize.y < gridSizeMin.y) {
                if (showErrors) {
                    Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage($"Min grid size = {gridSizeMin.x.ToString()}x{gridSizeMin.y.ToString()}");
                }

                return false;
            }

            if (gridSize.x > gridSizeMax.x || gridSize.y > gridSizeMax.y) {
                if (showErrors) {
                    Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage($"Max grid size = {gridSizeMax.x.ToString()}x{gridSizeMax.y.ToString()}");
                }

                return false;
            }

            return true;
        }

        private bool TryLoadProjectV1(ProjectJsonModel project) {
            Assert.IsTrue(project.Version == 1, "project.Version == 1");
            var data = JsonUtility.FromJson<DataV1JsonModel>(project.Data);

            var gridSize = new Vector2Int(data.Grid.GridSizeX, data.Grid.GridSizeZ);

            if (CheckGridSize(gridSize)) {
                model = new Fence(gridSize, data.Fence.TileIds);

                return true;
            }

            return false;
        }

        #endregion
    }
}