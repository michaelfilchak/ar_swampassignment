﻿using System.Collections.Generic;
using Game.Scripts.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace Game.Scripts.Services {
    public class FenceSquareViewerApiService : ApiServiceMonobeh {
        #region Unity

        [SerializeField]
        private GameObject fencePrefab;

        private void Awake() {
            cachedTransform = transform;
        }

        #endregion

        #region Public

        public void Disable() {
            fenceModel.TileIdAddedEvent -= OnTileIdAdded;
            fenceModel.TileIdRemovedEvent -= OnTileIdRemoved;
            fenceModel = null;

            Destroy(fenceParentTransform.gameObject);
            fenceParentTransform = null;
            Destroy(fencePoolParentTransform.gameObject);
            fencePoolParentTransform = null;

            fencePool = null;
            fenceByTileId = null;
        }

        public void Enable() {
            Assert.IsNull(fenceModel, "Double enable");
            fenceModel = fenceApiService.FenceModel;
            fenceModel.TileIdAddedEvent += OnTileIdAdded;
            fenceModel.TileIdRemovedEvent += OnTileIdRemoved;

            fenceParentTransform = new GameObject("Fence").transform;
            fencePoolParentTransform = new GameObject("FencePool").transform;
            fencePoolParentTransform.SetParent(cachedTransform);

            fencePool = new List<Transform>();
            fenceByTileId = new Dictionary<uint, Transform>();

            if (fenceModel.TileCount > 0) {
                var tileIds = new List<uint>(fenceModel.TileCount);
                fenceModel.GetTileIds(tileIds);

                foreach (var tileId in tileIds) {
                    OnTileIdAdded(tileId);
                }
            }
        }

        public override void Init() {
            base.Init();
            Api.Resolve(out fenceApiService);
        }

        #endregion

        #region Private

        private Dictionary<uint, Transform> fenceByTileId;
        private FenceApiService fenceApiService;
        private IFenceModel fenceModel;
        private List<Transform> fencePool;
        private Transform cachedTransform;
        private Transform fenceParentTransform;
        private Transform fencePoolParentTransform;

        private void OnTileIdAdded(uint value) {
            var fenceTransform = PopFenceTransform();
            fenceTransform.position = fenceApiService.ConvertTileIdToWorldCoordinates(value);
            fenceByTileId.Add(value, fenceTransform);
        }

        private void OnTileIdRemoved(uint value) {
            if (fenceByTileId.TryGetValue(value, out var fenceTransform)) {
                PushFenceTransform(fenceTransform);
                fenceByTileId.Remove(value);
            } else {
                Debug.LogError($"You want to remove not exist tileId={value.ToString()}");
            }
        }

        private Transform PopFenceTransform() {
            Transform fence;

            if (fencePool.Count == 0) {
                fence = Instantiate(fencePrefab).transform;
            } else {
                var lastIndex = fencePool.Count - 1;
                fence = fencePool[lastIndex];
                fence.gameObject.SetActive(true);
                fencePool.RemoveAt(lastIndex);
            }

            fence.SetParent(fenceParentTransform);

            return fence;
        }

        private void PushFenceTransform(Transform fenceTransform) {
            fenceTransform.gameObject.SetActive(false);
            fenceTransform.SetParent(fencePoolParentTransform);
            fenceTransform.position = Vector3.zero;
            fencePool.Add(fenceTransform);
        }

        #endregion
    }
}