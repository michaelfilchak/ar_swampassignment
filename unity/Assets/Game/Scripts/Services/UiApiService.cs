﻿using Game.Scripts.Core;
using UnityEngine;

namespace Game.Scripts.Services {
    public class UiApiService : ApiServiceMonobeh {
        #region Unity

        [SerializeField]
        private Transform layers;

        #endregion

        #region Public

        public void HideWindow<T>(T value)
            where T : Window {
            Destroy(value.gameObject);
        }

        public T ShowWindow<T>()
            where T : Window {
            var prefab = Resources.Load<T>(typeof(T).Name);
            var window = (T)Instantiate(prefab, layers);
            window.SetApi(Api);

            return window;
        }

        #endregion
    }
}