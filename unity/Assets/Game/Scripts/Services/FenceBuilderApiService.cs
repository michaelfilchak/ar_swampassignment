﻿using Game.Scripts.Core;
using UnityEngine;

namespace Game.Scripts.Services {
    public class FenceBuilderApiService : ApiService {
        #region Public

        public void Disable() {
            if (isEnabled) {
                isEnabled = false;
                pcFenceInputApiService.DownEvent -= OnDown;
                pcFenceInputApiService.MoveEvent -= OnMove;
            } else {
                Debug.LogError("Strange double Disable()");
            }
        }

        public void Enable() {
            if (isEnabled) {
                Debug.LogError("Strange double Enable()");
            } else {
                isEnabled = true;
                pcFenceInputApiService.DownEvent += OnDown;
                pcFenceInputApiService.MoveEvent += OnMove;
            }
        }

        public override void Init() {
            base.Init();
            Api.Resolve(out fenceApiService);
            Api.Resolve(out pcFenceInputApiService);
        }

        #endregion

        #region Private

        private bool isEnabled;
        private FenceApiService fenceApiService;
        private PcFenceInputApiService pcFenceInputApiService;

        private void OnDown() {
            fenceApiService.ProcessClickOnTile(pcFenceInputApiService.CurrentCoordinates);
        }

        private void OnMove() {
            fenceApiService.ProcessClickOnTile(pcFenceInputApiService.CurrentCoordinates);
        }

        #endregion
    }
}