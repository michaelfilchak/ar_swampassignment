﻿using Game.Scripts.Core;
using Game.Scripts.UtilsAndExts;
using Unity.Plastic.Antlr3.Runtime.Misc;
using UnityEngine;

namespace Game.Scripts.Services {
    public class PcFenceInputApiService : ApiServiceMonobeh {
        #region Unity

        [SerializeField]
        private Transform debugSphereTransform;

        private void Awake() {
            debugSphereTransform.gameObject.SetActive(false);
            gameObject.SetActive(Utils.Application.IsEditorOrStandalone);
            Disable();
        }

        private void Update() {
            var ray = targetCamera.ScreenPointToRay(Input.mousePosition);
            var rayWithDownDot = Vector3.Dot(ray.direction, Vector3.down);
            var x = -1000;
            var y = -1000;

            if (rayWithDownDot > 0) {
                var rayLengthToGround = ray.origin.y / rayWithDownDot;
                var positionOnPlane = ray.origin + ray.direction * rayLengthToGround;
                x = (int)positionOnPlane.x;
                y = (int)positionOnPlane.z;
            } else {
                Debug.LogError("Dot of Ray with Down is zero");
            }

            // Intersection of the ray with the ground
            if (Input.GetMouseButton(0)) {
                if (x >= 0 && y >= 0) {
                    if (isDown) {
                        if (currentCoordinates.x != x || currentCoordinates.y != y) {
                            isMoved = true;
                            SetCoordinates(x, y);
                            MoveEvent?.Invoke();
                        }
                    } else {
                        isDown = true;
                        isMoved = false;
                        SetCoordinates(x, y);
                        DownEvent?.Invoke();
                    }
                }
            } else if (isDown) {
                isDown = false;
                UpEvent?.Invoke();

                if (!isMoved) {
                    ClickEvent?.Invoke();
                }
            }

            if (isDebugMoveEnabled && rayWithDownDot > 0) {
                if (debugMoveCoordinates.x != x || debugMoveCoordinates.y != y) {
                    debugMoveCoordinates.x = x;
                    debugMoveCoordinates.y = y;

                    debugSphereTransform.position = new Vector3(x + 0.5f, 0, y + 0.5f);
                }
            }
        }

        #endregion

        #region Public

        public event Action ClickEvent;
        public event Action DownEvent;
        public event Action MoveEvent;
        public event Action UpEvent;
        public ref Vector2Int CurrentCoordinates => ref currentCoordinates;

        public void Disable() {
            if (Utils.Application.IsEditorOrStandalone) {
                gameObject.SetActive(false);
            }
        }

        public void DisableDebugMove() {
            if (isDebugMoveEnabled) {
                isDebugMoveEnabled = false;
                debugSphereTransform.gameObject.SetActive(false);
            } else {
                Debug.LogError("Double disable debug move");
            }
        }

        public void Enable() {
            if (Utils.Application.IsEditorOrStandalone) {
                gameObject.SetActive(true);
                SetCoordinates(-1, -1);
            }
        }

        public void EnableDebugMove() {
            if (isDebugMoveEnabled) {
                Debug.LogError("Double enable debug move");
            } else {
                isDebugMoveEnabled = true;
                debugSphereTransform.gameObject.SetActive(true);
                debugMoveCoordinates = new Vector2Int(-10000, -10000);
            }
        }

        public override void Init() {
            base.Init();
            targetCamera = Api.Resolve<CameraApiService>().CachedCamera;
        }

        public void SetCoordinates(int x, int y) {
            currentCoordinates.x = x;
            currentCoordinates.y = y;
        }

        #endregion

        #region Private

        private bool isDebugMoveEnabled;
        private bool isDown;
        private bool isMoved;
        private Camera targetCamera;
        private Vector2Int currentCoordinates;
        private Vector2Int debugMoveCoordinates;

        #endregion
    }
}