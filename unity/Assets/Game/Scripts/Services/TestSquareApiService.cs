﻿using Game.Scripts.Core;
using UnityEngine;

namespace Game.Scripts.Services {
    public class TestSquareApiService : ApiServiceMonobeh {
        #region Unity

        [SerializeField]
        private Transform emptyTileTransform;
        [SerializeField]
        private Transform fenceTileTransform;
        [SerializeField]
        private Transform insideFenceTileTransform;

        private void Awake() {
            emptyTileTransform.gameObject.SetActive(false);
            fenceTileTransform.gameObject.SetActive(false);
            insideFenceTileTransform.gameObject.SetActive(false);
        }

        #endregion

        #region Public

        public void Disable() {
            if (isEnabled) {
                isEnabled = false;
                gameObject.SetActive(false);
                pcFenceInputApiService.DownEvent -= OnDown;
                pcFenceInputApiService.MoveEvent -= OnMove;
                pcFenceInputApiService.UpEvent -= OnUp;
            } else {
                Debug.LogError("Double disable");
            }
        }

        public void Enable() {
            if (isEnabled) {
                Debug.LogError("Double enable");
            } else {
                isEnabled = true;
                gameObject.SetActive(true);
                pcFenceInputApiService.DownEvent += OnDown;
                pcFenceInputApiService.MoveEvent += OnMove;
                pcFenceInputApiService.UpEvent += OnUp;
                ProcessCoordinates(pcFenceInputApiService.CurrentCoordinates);
            }
        }

        public override void Init() {
            base.Init();
            gameObject.SetActive(false);
            Api.Resolve(out pcFenceInputApiService);
            Api.Resolve(out fenceApiService);
        }

        #endregion

        #region Private

        private bool isEnabled;
        private FenceApiService fenceApiService;
        private PcFenceInputApiService pcFenceInputApiService;
        private Transform currentTransform;

        private void ActivateTransform(Transform value) {
            if (currentTransform == value) {
                return;
            }

            if (currentTransform != null) {
                currentTransform.gameObject.SetActive(false);
            }

            currentTransform = value;

            if (currentTransform != null) {
                currentTransform.gameObject.SetActive(true);
            }
        }

        private void OnDown() {
            ProcessCoordinates(pcFenceInputApiService.CurrentCoordinates);
        }

        private void OnMove() {
            ProcessCoordinates(pcFenceInputApiService.CurrentCoordinates);
        }

        private void OnUp() {
            ActivateTransform(null);
        }

        private void ProcessCoordinates(in Vector2Int value) {
            if (fenceApiService.FenceModel.IsInGridRange(value.x, value.y)) {
                Fence.GetFenceTileId((ushort)value.x, (ushort)value.y, out var tileId);

                if (fenceApiService.FenceModel.ContainsTileId(tileId)) {
                    ActivateTransform(fenceTileTransform);
                } else if (fenceApiService.IsFreeTileId(tileId)) {
                    ActivateTransform(emptyTileTransform);
                } else {
                    ActivateTransform(insideFenceTileTransform);
                }

                currentTransform.position = fenceApiService.ConvertTileIdToWorldCoordinates(tileId);
            } else {
                ActivateTransform(null);
            }
        }

        #endregion
    }
}