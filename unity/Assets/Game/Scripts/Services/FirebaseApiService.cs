﻿using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using Firebase;
using Firebase.Extensions;
using Firebase.Firestore;
using Game.Scripts.Core;
using Game.Scripts.Windows;
using UnityEngine;

namespace Game.Scripts.Services {
    public class FirebaseApiService : ApiService {
        #region Public

        public override void Init() {
            base.Init();
            FirebaseApp.CheckAndFixDependenciesAsync()
                .ContinueWithOnMainThread(
                    task => {
                        var dependencyStatus = task.Result;

                        if (dependencyStatus == DependencyStatus.Available) {
                            db = FirebaseFirestore.DefaultInstance;
                        } else {
                            Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
                        }
                    });
        }

        public async UniTask<string> LoadData(CancellationToken cancellationToken) {
            if (db == null) {
                Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage("Firebase is not ready");

                return null;
            }

            var collectionReference = db.Collection("fence_remote");
            var snapshot = await collectionReference.GetSnapshotAsync();
            cancellationToken.ThrowIfCancellationRequested();

            foreach (var document in snapshot.Documents) {
                var documentDictionary = document.ToDictionary();

                if (documentDictionary.TryGetValue(Key, out var jsonObject)) {
                    var json = (string)jsonObject;

                    return json;
                }
            }

            return null;
        }

        public void SaveData(string value) {
            if (db == null) {
                Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage("Firebase is not ready");

                return;
            }

            SaveDataInner(value).Forget();
        }

        #endregion

        #region Private

        private const string Key = "ProjectJson";
        private FirebaseFirestore db;

        private async UniTaskVoid SaveDataInner(string value) {
            var docRef = db.Collection("fence_remote").Document("fence_remote_doc");
            var documentDictionary = new Dictionary<string, object> {
                { Key, value }
            };
            await docRef.SetAsync(documentDictionary);
            Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage("Data was saved");
        }

        #endregion
    }
}