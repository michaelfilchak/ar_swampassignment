using System;
using System.Collections.Generic;
using Game.Scripts.Core;
using Game.Scripts.Services;
using Game.Scripts.UtilsAndExts;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace Game.Scripts {
    public class Launcher : MonoBehaviour {
        #region Unity

        private void Awake() {
            CreateApi();
        }

        private void Start() {
            api.Resolve<GameFsmApiService>().Launch();
        }

        #endregion

        #region Private

        private Api api;

        private void CreateApi() {
            Assert.IsNull(api);

            var apiServiceList = new List<IApiService>();

            { // ApiService
                var typeList = new List<Type>();
                Utils.Assembly.AddSubclassesThroughHierarchy<ApiService>(typeList);

                foreach (var type in typeList) {
                    var apiService = (ApiService)Activator.CreateInstance(type);
                    apiServiceList.Add(apiService);
                }
            }

            { // ApiServiceMonobeh
                var rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();

                foreach (var rootGameObject in rootGameObjects) {
                    var apiServiceMonobehs = rootGameObject.GetComponentsInChildren<ApiServiceMonobeh>(true);
                    apiServiceList.AddRange(apiServiceMonobehs);
                }
            }

            api = Api.CreateApi(apiServiceList);
        }

        #endregion
    }
}