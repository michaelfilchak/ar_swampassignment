﻿using Game.Scripts.Services;
using Game.Scripts.Windows;

namespace Game.Scripts.GameFsm {
    public class TestSquareFsmState : GameFsmState {
        #region Public

        public override void Enter() {
            base.Enter();
            testSquareMenuWindow = Api.Resolve<UiApiService>().ShowWindow<TestSquareMenuWindow>();
            testSquareMenuWindow.BackButtonClickedEvent += OnBackButtonClicked;

            var groundApiService = Api.Resolve<GroundApiService>();
            var fenceApiService = Api.Resolve<FenceApiService>();
            var gridSize = fenceApiService.FenceModel.GridSize;
            groundApiService.Show(gridSize.x, gridSize.y);

            Api.Resolve<PcFenceInputApiService>().Enable();
            Api.Resolve<FenceSquareViewerApiService>().Enable();
            Api.Resolve<TestSquareApiService>().Enable();

            if (!Api.Resolve<FenceApiService>().IsFenceAreaLocked()) {
                var notificationWindow = Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>();
                notificationWindow.ShowMessage("Fence is not locked\nAll tiles are open (except fences)");
            }
        }

        public override void Exit() {
            base.Exit();
            testSquareMenuWindow.BackButtonClickedEvent -= OnBackButtonClicked;
            testSquareMenuWindow.Close();

            Api.Resolve<TestSquareApiService>().Disable();
            Api.Resolve<FenceSquareViewerApiService>().Disable();
            Api.Resolve<GroundApiService>().Hide();
            Api.Resolve<PcFenceInputApiService>().Disable();
        }

        #endregion

        #region Private

        private TestSquareMenuWindow testSquareMenuWindow;

        private void OnBackButtonClicked() {
            Fsm.ActiveFsmState<FenceMenuFsmState>();
        }

        #endregion
    }
}