﻿using Game.Scripts.Services;
using Game.Scripts.Windows;

namespace Game.Scripts.GameFsm {
    public class EditFenceFsmState : GameFsmState {
        #region Public

        public override void Enter() {
            base.Enter();
            var fenceApiService = Api.Resolve<FenceApiService>();
            var gridSize = fenceApiService.FenceModel.GridSize;
            Api.Resolve<GroundApiService>().Show(gridSize.x, gridSize.y);

            Api.Resolve<PcFenceInputApiService>().Enable();
            Api.Resolve<FenceSquareViewerApiService>().Enable();
            Api.Resolve<FenceBuilderApiService>().Enable();

            fenceEditWindow = Api.Resolve<UiApiService>().ShowWindow<FenceEditWindow>();
            fenceEditWindow.SaveLocallyAndBackButtonClickedEvent += OnSaveLocallyAndBackButtonClicked;
            fenceEditWindow.SaveRemoteAndBackButtonClickedEvent += OnSaveRemoteAndBackButtonClicked;
        }

        public override void Exit() {
            base.Exit();
            fenceEditWindow.SaveLocallyAndBackButtonClickedEvent -= OnSaveLocallyAndBackButtonClicked;
            fenceEditWindow.SaveRemoteAndBackButtonClickedEvent -= OnSaveRemoteAndBackButtonClicked;
            fenceEditWindow.Close();

            Api.Resolve<FenceSquareViewerApiService>().Disable();
            Api.Resolve<GroundApiService>().Hide();
            Api.Resolve<PcFenceInputApiService>().Disable();
            Api.Resolve<FenceBuilderApiService>().Disable();
        }

        #endregion

        #region Private

        private FenceEditWindow fenceEditWindow;

        private void OnSaveLocallyAndBackButtonClicked() {
            Api.Resolve<FenceApiService>().SaveModelLocally();
            Fsm.ActiveFsmState<FenceMenuFsmState>();
        }

        private void OnSaveRemoteAndBackButtonClicked() {
            var json = Api.Resolve<FenceApiService>().GetJson();
            Api.Resolve<FirebaseApiService>().SaveData(json);
            Fsm.ActiveFsmState<FenceMenuFsmState>();
        }

        #endregion
    }
}