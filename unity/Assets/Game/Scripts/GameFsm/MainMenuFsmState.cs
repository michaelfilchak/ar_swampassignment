﻿using System.Threading;
using Cysharp.Threading.Tasks;
using Game.Scripts.Services;
using Game.Scripts.Windows;

namespace Game.Scripts.GameFsm {
    public class MainMenuFsmState : GameFsmState {
        #region Public

        public override void Enter() {
            base.Enter();
            mainMenuWindow = Api.Resolve<UiApiService>().ShowWindow<MainMenuWindow>();
            mainMenuWindow.LoadLocalButtonClickedEvent += OnLoadLocalButtonClicked;
            mainMenuWindow.LoadRemoteButtonClickedEvent += OnLoadRemoteButtonClicked;
            mainMenuWindow.CreateNewButtonClickedEvent += OnCreateNewButtonClicked;

            var fenceApiService = Api.Resolve<FenceApiService>();

            if (fenceApiService.IsModelReady) {
                fenceApiService.CloseModel();
            }
        }

        public override void Exit() {
            base.Exit();
            mainMenuWindow.LoadLocalButtonClickedEvent -= OnLoadLocalButtonClicked;
            mainMenuWindow.LoadRemoteButtonClickedEvent -= OnLoadRemoteButtonClicked;
            mainMenuWindow.CreateNewButtonClickedEvent -= OnCreateNewButtonClicked;
            mainMenuWindow.Close();

            if (cancellationTokenSource != null) {
                cancellationTokenSource.Cancel();
                cancellationTokenSource.Dispose();
                cancellationTokenSource = null;
            }
        }

        #endregion

        #region Private

        private CancellationTokenSource cancellationTokenSource;
        private MainMenuWindow mainMenuWindow;

        private async UniTask LoadRemoveDataAsync(CancellationToken cancellationToken) {
            var json = await Api.Resolve<FirebaseApiService>().LoadData(cancellationToken);

            if (string.IsNullOrEmpty(json)) {
                Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>().ShowMessage("Remote data is null");
            } else {
                var fenceApiService = Api.Resolve<FenceApiService>();

                if (fenceApiService.TryLoadModelFromJson(json)) {
                    Fsm.ActiveFsmState<FenceMenuFsmState>();
                }
            }
        }

        private void OnCreateNewButtonClicked() {
            Fsm.ActiveFsmState<CreateNewFenceFsmState>();
        }

        private void OnLoadLocalButtonClicked() {
            var fenceApiService = Api.Resolve<FenceApiService>();

            if (fenceApiService.TryLoadModelLocally(fenceApiService.LocalFencePath)) {
                Fsm.ActiveFsmState<FenceMenuFsmState>();
            }
        }

        private void OnLoadRemoteButtonClicked() {
            if (cancellationTokenSource != null) {
                cancellationTokenSource.Cancel();
                cancellationTokenSource.Dispose();
                cancellationTokenSource = null;
            }

            cancellationTokenSource = new CancellationTokenSource();
            LoadRemoveDataAsync(cancellationTokenSource.Token).Forget();
        }

        #endregion
    }
}