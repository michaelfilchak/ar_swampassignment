﻿using Game.Scripts.Services;
using Game.Scripts.Windows;
using UnityEngine;

namespace Game.Scripts.GameFsm {
    public class CreateNewFenceFsmState : GameFsmState {
        #region Public

        public override void Enter() {
            base.Enter();
            Api.Resolve(out fenceApiService);
            gridSizeWindow = Api.Resolve<UiApiService>().ShowWindow<GridSizeWindow>();
            gridSizeWindow.CreateButtonEvent += OnCreateButton;
        }

        public override void Exit() {
            base.Exit();
            gridSizeWindow.CreateButtonEvent -= OnCreateButton;
            gridSizeWindow.Close();
        }

        #endregion

        #region Private

        private FenceApiService fenceApiService;
        private GridSizeWindow gridSizeWindow;

        private void OnCreateButton(Vector2Int gridSize) {
            if (fenceApiService.TryCreateModel(gridSize)) {
                Fsm.ActiveFsmState<FenceMenuFsmState>();
            }
        }

        #endregion
    }
}