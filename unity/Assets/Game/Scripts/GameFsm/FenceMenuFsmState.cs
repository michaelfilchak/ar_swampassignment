﻿using Game.Scripts.Services;
using Game.Scripts.Windows;

namespace Game.Scripts.GameFsm {
    public class FenceMenuFsmState : GameFsmState {
        #region Public

        public override void Enter() {
            base.Enter();
            fenceMenuWindow = Api.Resolve<UiApiService>().ShowWindow<FenceMenuWindow>();
            fenceMenuWindow.BackButtonClickedEvent += OnBackButtonClicked;
            fenceMenuWindow.EditFenceButtonClickedEvent += OnEditFenceButtonClicked;
            fenceMenuWindow.TestFenceButtonClickedEvent += OnTestFenceButtonClicked;
            fenceMenuWindow.TestSquareButtonClickedEvent += OnTestSquareButtonClicked;

            Api.Resolve(out groundApiService);
            var fenceApiService = Api.Resolve<FenceApiService>();
            var gridSize = fenceApiService.FenceModel.GridSize;
            groundApiService.Show(gridSize.x, gridSize.y);

            Api.Resolve(out pcFenceInputApiService);
            pcFenceInputApiService.Enable();
            pcFenceInputApiService.EnableDebugMove();

            Api.Resolve(out fenceSquareViewerApiService);
            fenceSquareViewerApiService.Enable();
        }

        public override void Exit() {
            base.Exit();
            fenceMenuWindow.BackButtonClickedEvent -= OnBackButtonClicked;
            fenceMenuWindow.EditFenceButtonClickedEvent -= OnEditFenceButtonClicked;
            fenceMenuWindow.TestFenceButtonClickedEvent -= OnTestFenceButtonClicked;
            fenceMenuWindow.TestSquareButtonClickedEvent -= OnTestSquareButtonClicked;
            fenceMenuWindow.Close();

            fenceSquareViewerApiService.Disable();
            groundApiService.Hide();
            pcFenceInputApiService.Disable();
            pcFenceInputApiService.DisableDebugMove();
        }

        #endregion

        #region Private

        private FenceMenuWindow fenceMenuWindow;
        private FenceSquareViewerApiService fenceSquareViewerApiService;
        private GroundApiService groundApiService;
        private PcFenceInputApiService pcFenceInputApiService;

        private void OnBackButtonClicked() {
            Fsm.ActiveFsmState<MainMenuFsmState>();
        }

        private void OnEditFenceButtonClicked() {
            Fsm.ActiveFsmState<EditFenceFsmState>();
        }

        private void OnTestFenceButtonClicked() {
            var notificationWindow = Api.Resolve<UiApiService>().ShowWindow<NotificationWindow>();
            var message = Api.Resolve<FenceApiService>().IsFenceAreaLocked() ? "Fence is locked" : "Fence is not locked";
            notificationWindow.ShowMessage(message);
        }

        private void OnTestSquareButtonClicked() {
            Fsm.ActiveFsmState<TestSquareFsmState>();
        }

        #endregion
    }
}