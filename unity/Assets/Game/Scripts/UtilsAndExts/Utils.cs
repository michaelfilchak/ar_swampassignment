﻿namespace Game.Scripts.UtilsAndExts {
    public static class Utils {
        #region Public

        public static readonly ApplicationUtility Application = new ApplicationUtility();
        public static readonly AssemblyUtility Assembly = new AssemblyUtility();

        #endregion
    }
}