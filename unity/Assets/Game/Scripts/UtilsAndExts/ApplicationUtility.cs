﻿using UnityEngine;

namespace Game.Scripts.UtilsAndExts {
    public class ApplicationUtility {
        #region Public

        public bool IsEditorOrStandalone { get; private set; } = Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.LinuxPlayer;

        #endregion
    }
}