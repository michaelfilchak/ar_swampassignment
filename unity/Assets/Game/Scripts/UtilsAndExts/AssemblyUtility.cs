﻿using System;
using System.Collections.Generic;
using UnityEngine.Assertions;

namespace Game.Scripts.UtilsAndExts {
    public class AssemblyUtility {
        #region Setup/Teardown

        static AssemblyUtility() {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var typeList = new List<Type>();

            foreach (var assembly in assemblies) {
                var types = assembly.GetTypes();
                typeList.AddRange(types);
            }

            AllTypes = typeList.ToArray();
            AllTypeCount = AllTypes.Length;
        }

        #endregion

        #region Public

        public void AddSubclasses<T>(List<Type> resultTypeList, bool inclusiveAbstract = true) {
            AddSubclasses(resultTypeList, typeof(T), inclusiveAbstract);
        }

        public void AddSubclasses(List<Type> resultTypeList, Type baseType, bool inclusiveAbstract = true) {
            Assert.IsNotNull(resultTypeList);
            Assert.IsNotNull(baseType);
            Assert.IsNull(typeToCheck0);
            Assert.IsTrue(index0 == -1);
            index0 = 0;

            if (inclusiveAbstract) {
                while (index0 < AllTypeCount) {
                    typeToCheck0 = AllTypes[index0];

                    if (typeToCheck0.BaseType == baseType) {
                        resultTypeList.Add(typeToCheck0);
                    }

                    index0++;
                }
            } else {
                while (index0 < AllTypeCount) {
                    typeToCheck0 = AllTypes[index0];

                    if (!typeToCheck0.IsAbstract && typeToCheck0.BaseType == baseType) {
                        resultTypeList.Add(typeToCheck0);
                    }

                    index0++;
                }
            }

            index0 = -1;
            typeToCheck0 = null;
        }

        public void AddSubclassesThroughHierarchy(List<Type> resultTypeList, Type baseType, bool inclusiveAbstract = true) {
            Assert.IsNotNull(resultTypeList);
            Assert.IsNotNull(baseType);
            Assert.IsNull(typeToCheck0);
            Assert.IsTrue(index0 == -1);
            index0 = 0;

            if (inclusiveAbstract) {
                while (index0 < AllTypeCount) {
                    typeToCheck0 = AllTypes[index0];

                    if (IsSubclassThroughHierarchy(baseType, typeToCheck0)) {
                        resultTypeList.Add(typeToCheck0);
                    }

                    index0++;
                }
            } else {
                while (index0 < AllTypeCount) {
                    typeToCheck0 = AllTypes[index0];

                    if (!typeToCheck0.IsAbstract && IsSubclassThroughHierarchy(baseType, typeToCheck0)) {
                        resultTypeList.Add(typeToCheck0);
                    }

                    index0++;
                }
            }

            index0 = -1;
            typeToCheck0 = null;
        }

        public void AddSubclassesThroughHierarchy<T>(List<Type> resultTypeList, bool inclusiveAbstract = true) {
            AddSubclassesThroughHierarchy(resultTypeList, typeof(T), inclusiveAbstract);
        }

        public bool IsSubclassThroughHierarchy(Type baseType, Type subclassType) {
            Assert.IsNotNull(baseType);
            Assert.IsNotNull(subclassType);
            Assert.IsNull(typeToCheck1);

            if (baseType == subclassType) {
                return false;
            }

            // way #1
            //return subclassType.IsSubclassOf(baseType);
            // way #2
            typeToCheck1 = subclassType.BaseType;

            while (typeToCheck1 != null) {
                if (typeToCheck1 == baseType) {
                    typeToCheck1 = null;

                    return true;
                }

                typeToCheck1 = typeToCheck1.BaseType;
            }

            typeToCheck1 = null;

            return false;
        }

        #endregion

        #region Private

        private static readonly int AllTypeCount;
        private static readonly Type[] AllTypes;
        private int index0 = -1;
        private Type typeToCheck0;
        private Type typeToCheck1;

        #endregion
    }
}