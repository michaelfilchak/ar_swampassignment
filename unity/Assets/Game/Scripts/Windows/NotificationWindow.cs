﻿using Game.Scripts.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Windows {
    public class NotificationWindow : Window {
        #region Unity

        [SerializeField]
        private RectTransform panelRectTransform;
        [SerializeField]
        private TMP_Text labelText;
        [SerializeField]
        private Button okButton;

        private void Awake() {
            okButton.onClick.AddListener(Close);
        }

        #endregion

        #region Public

        public void ShowMessage(string value) {
            var width = labelText.GetPreferredValues(value).x;
            width += 40;

            if (width < 300) {
                width = 300;
            }

            var sizeDelta = panelRectTransform.sizeDelta;
            sizeDelta.x = width;
            panelRectTransform.sizeDelta = sizeDelta;

            labelText.text = value;
        }

        #endregion
    }
}