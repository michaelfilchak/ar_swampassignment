using System;
using Game.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Windows {
    public class MainMenuWindow : Window {
        #region Unity

        [SerializeField]
        private Button loadLocalButton;
        [SerializeField]
        private Button loadRemoteButton;
        [SerializeField]
        private Button createNewButton;

        private void Awake() {
            loadLocalButton.onClick.AddListener(() => LoadLocalButtonClickedEvent?.Invoke());
            loadRemoteButton.onClick.AddListener(() => LoadRemoteButtonClickedEvent?.Invoke());
            createNewButton.onClick.AddListener(() => CreateNewButtonClickedEvent?.Invoke());
        }

        #endregion

        #region Public

        public event Action CreateNewButtonClickedEvent;
        public event Action LoadLocalButtonClickedEvent;
        public event Action LoadRemoteButtonClickedEvent;

        #endregion
    }
}