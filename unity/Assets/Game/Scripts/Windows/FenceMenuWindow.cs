﻿using System;
using Game.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Windows {
    public class FenceMenuWindow : Window {
        #region Unity

        [SerializeField]
        private Button backButton;
        [SerializeField]
        private Button editFenceButton;
        [SerializeField]
        private Button testFenceButton;
        [SerializeField]
        private Button testSquareButton;

        private void Awake() {
            backButton.onClick.AddListener(() => BackButtonClickedEvent?.Invoke());
            editFenceButton.onClick.AddListener(() => EditFenceButtonClickedEvent?.Invoke());
            testFenceButton.onClick.AddListener(() => TestFenceButtonClickedEvent?.Invoke());
            testSquareButton.onClick.AddListener(() => TestSquareButtonClickedEvent?.Invoke());
        }

        #endregion

        #region Public

        public event Action BackButtonClickedEvent;
        public event Action EditFenceButtonClickedEvent;
        public event Action TestFenceButtonClickedEvent;
        public event Action TestSquareButtonClickedEvent;

        #endregion
    }
}