﻿using System;
using Game.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Windows {
    public class TestSquareMenuWindow : Window {
        #region Unity

        [SerializeField]
        private Button backButton;

        private void Awake() {
            backButton.onClick.AddListener(() => BackButtonClickedEvent?.Invoke());
        }

        #endregion

        #region Public

        public event Action BackButtonClickedEvent;

        #endregion
    }
}