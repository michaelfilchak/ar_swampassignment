﻿using System;
using Game.Scripts.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Windows {
    public class GridSizeWindow : Window {
        #region Unity

        [SerializeField]
        private TMP_InputField xInputField;
        [SerializeField]
        private TMP_InputField zInputField;
        [SerializeField]
        private Button button;

        private void Awake() {
            button.onClick.AddListener(OnButtonClicked);
        }

        #endregion

        #region Public

        public event Action<Vector2Int> CreateButtonEvent;

        #endregion

        #region Private

        private void OnButtonClicked() {
            if (int.TryParse(xInputField.text, out var x)) {
                if (int.TryParse(zInputField.text, out var z)) {
                    CreateButtonEvent?.Invoke(new Vector2Int(x, z));
                } else {
                    CachedUiApiService.ShowWindow<NotificationWindow>().ShowMessage("Couldn't parse Z");
                }
            } else {
                CachedUiApiService.ShowWindow<NotificationWindow>().ShowMessage("Couldn't parse X");
            }
        }

        #endregion
    }
}