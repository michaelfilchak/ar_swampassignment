﻿using System;
using Game.Scripts.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Windows {
    public class FenceEditWindow : Window {
        #region Unity

        [SerializeField]
        private Button saveLocallyButton;
        [SerializeField]
        private Button saveRemoteButton;

        private void Awake() {
            saveLocallyButton.onClick.AddListener(() => SaveLocallyAndBackButtonClickedEvent?.Invoke());
            saveRemoteButton.onClick.AddListener(() => SaveRemoteAndBackButtonClickedEvent?.Invoke());
        }

        #endregion

        #region Public

        public event Action SaveRemoteAndBackButtonClickedEvent;
        public event Action SaveLocallyAndBackButtonClickedEvent;

        #endregion
    }
}