﻿using Game.Scripts.Services;
using UnityEngine;

namespace Game.Scripts.Core {
    public abstract class Window : MonoBehaviour {
        #region Public

        public void Close() {
            Api.Resolve<UiApiService>().HideWindow(this);
        }

        public void SetApi(Api value) {
            Api = value;
            CachedUiApiService = Api.Resolve<UiApiService>();
        }

        #endregion

        #region Protected

        protected Api Api { get; private set; }
        protected UiApiService CachedUiApiService { get; private set; }

        #endregion
    }
}