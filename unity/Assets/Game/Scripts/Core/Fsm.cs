﻿using System;
using System.Collections.Generic;
using Game.Scripts.UtilsAndExts;
using UnityEngine.Assertions;

namespace Game.Scripts.Core {
    public class Fsm<T> : IFsm
        where T : FsmState {
        #region Setup/Teardown

        public Fsm() {
            var typeList = new List<Type>();
            Utils.Assembly.AddSubclassesThroughHierarchy<T>(typeList, false);
            fsmStateByType = new Dictionary<Type, T>(typeList.Count);

            foreach (var type in typeList) {
                var fsmState = (T)Activator.CreateInstance(type);
                fsmState.SetFsm(this);
                fsmStateByType.Add(type, fsmState);
            }
        }

        #endregion

        #region IFsm

        public void ActiveFsmState<TState>()
            where TState : FsmState {
            active.Exit();
            active = fsmStateByType[typeof(TState)];
            active.Enter();
        }

        #endregion

        #region Public

        public void Launch<TState>()
            where TState : T {
            Assert.IsNull(active);
            active = (TState)fsmStateByType[typeof(TState)];
            active.Enter();
        }

        public void SetApi(Api value) {
            foreach (var fsmState in fsmStateByType.Values) {
                fsmState.SetApi(value);
            }
        }

        #endregion

        #region Private

        private readonly Dictionary<Type, T> fsmStateByType;
        private T active;

        #endregion
    }
}