﻿namespace Game.Scripts.Core {
    public interface IApiService {
        #region Public

        void Init();
        void SetApi(Api value);

        #endregion
    }
}