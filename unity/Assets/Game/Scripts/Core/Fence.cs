﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Scripts.Core {
    public class Fence : IFenceModel {
        #region Setup/Teardown

        public Fence(in Vector2Int gridSize) {
            GridSize = gridSize;
            gridSizeMinusOne = GridSize - Vector2Int.one;
        }

        public Fence(in Vector2Int gridSize, IEnumerable<uint> tileIds) : this(gridSize) {
            fenceTileIds.UnionWith(tileIds);
        }

        #endregion

        #region IFenceModel

        public bool ContainsTileId(uint value) {
            return fenceTileIds.Contains(value);
        }

        public void GetTileIds(List<uint> value) {
            value.AddRange(fenceTileIds);
        }

        public bool IsInGridRange(int x, int z) {
            return 0 <= x && x < GridSize.x && 0 <= z && z < GridSize.y;
        }

        public event Action<uint> TileIdAddedEvent;
        public event Action<uint> TileIdRemovedEvent;
        public int TileCount => fenceTileIds.Count;
        public Vector2Int GridSize { get; }

        #endregion

        #region Public

        public string FileName { get; private set; }

        public static void ConvertFenceTileIdToGridCoordinates(in uint tileId, out ushort x, out ushort z) {
            x = (ushort)(tileId >> 16);
            z = (ushort)(tileId);
        }

        public static uint GetFenceTileId(in ushort x, in ushort z) {
            return ((uint)x << 16) | z;
        }

        public static void GetFenceTileId(in ushort x, in ushort z, out uint tileId) {
            tileId = ((uint)x << 16) | z;
        }

        public uint[] GetTileIdArray() {
            return fenceTileIds.ToArray();
        }

        public void ProcessClickOnTile(in Vector2Int value) {
            if (value.x < 1 || value.y < 1 || value.x >= gridSizeMinusOne.x || value.y >= gridSizeMinusOne.y) { // click is out of grid range
                return;
            }

            var tileId = GetFenceTileId((ushort)value.x, (ushort)value.y);

            if (fenceTileIds.Add(tileId)) {
                TileIdAddedEvent?.Invoke(tileId);
            } else {
                fenceTileIds.Remove(tileId);
                TileIdRemovedEvent?.Invoke(tileId);
            }
        }

        public void SetFileName(string value) {
            FileName = value;
        }

        #endregion

        #region Private

        private readonly HashSet<uint> fenceTileIds = new HashSet<uint>();
        private Vector2Int gridSizeMinusOne;

        #endregion
    }
}