﻿using System;
using System.Collections.Generic;

namespace Game.Scripts.Core {
    public class Api {
        #region Public

        public static Api CreateApi(List<IApiService> apiServices) {
            var instance = new Api {
                apiServiceByType = new Dictionary<Type, IApiService>(apiServices.Count)
            };

            foreach (var apiService in apiServices) {
                var type = apiService.GetType();
                instance.apiServiceByType.Add(type, apiService);
            }

            foreach (var apiService in apiServices) {
                apiService.SetApi(instance);
            }

            foreach (var apiService in apiServices) {
                apiService.Init();
            }

            return instance;
        }

        public T Resolve<T>()
            where T : IApiService {
            return (T)apiServiceByType[typeof(T)];
        }

        public void Resolve<T>(out T apiService)
            where T : IApiService {
            apiService = (T)apiServiceByType[typeof(T)];
        }

        #endregion

        #region Private

        private Dictionary<Type, IApiService> apiServiceByType;

        #endregion
    }
}