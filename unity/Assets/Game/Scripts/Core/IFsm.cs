﻿namespace Game.Scripts.Core {
    public interface IFsm {
        #region Public

        public void ActiveFsmState<T>()
            where T : FsmState;

        #endregion
    }
}