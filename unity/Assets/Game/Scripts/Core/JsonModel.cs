﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Game.Scripts.Core {
    [Serializable]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class DataV1JsonModel {
        #region Unity

        public FenceJsonModel Fence = new FenceJsonModel();
        public GridJsonModel Grid = new GridJsonModel();

        #endregion
    }

    [Serializable]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class FenceJsonModel {
        #region Unity

        public uint[] TileIds;

        #endregion
    }

    [Serializable]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class GridJsonModel {
        #region Unity

        public ushort GridSizeX;
        public ushort GridSizeZ;

        #endregion
    }

    [Serializable]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ProjectJsonModel {
        #region Unity

        public string Data;
        public byte Version;

        #endregion
    }
}