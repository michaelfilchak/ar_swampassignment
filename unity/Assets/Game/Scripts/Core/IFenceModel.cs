﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Scripts.Core {
    public interface IFenceModel {
        #region Public

        event Action<uint> TileIdAddedEvent;
        event Action<uint> TileIdRemovedEvent;
        int TileCount { get; }
        Vector2Int GridSize { get; }
        bool ContainsTileId(uint value);
        void GetTileIds(List<uint> value);
        bool IsInGridRange(int x, int z);

        #endregion
    }
}