﻿using UnityEngine.Assertions;

namespace Game.Scripts.Core {
    public abstract class FsmState {
        #region Public

        public virtual void Enter() {
        }

        public virtual void Exit() {
        }

        public void SetApi(Api value) {
            Api = value;
        }

        public void SetFsm(IFsm value) {
            Assert.IsNull(Fsm);
            Assert.IsNotNull(value);
            Fsm = value;
        }

        #endregion

        #region Protected

        protected Api Api { get; private set; }
        protected IFsm Fsm { get; private set; }

        #endregion
    }
}