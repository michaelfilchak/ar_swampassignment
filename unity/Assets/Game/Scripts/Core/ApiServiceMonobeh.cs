﻿using UnityEngine;

namespace Game.Scripts.Core {
    public abstract class ApiServiceMonobeh : MonoBehaviour,
        IApiService {
        #region IApiService

        public void SetApi(Api value) {
            Api = value;
        }

        public virtual void Init() {
        }

        #endregion

        #region Protected

        protected Api Api { get; private set; }

        #endregion
    }
}